import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SabreTableComponent } from './sabre-table/sabre-table.component';
import { SabreChartComponent } from './sabre-chart/sabre-chart.component';

const routes: Routes = [
  { path: 'chart', component: SabreChartComponent },
  { path: '', component: SabreTableComponent   }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
