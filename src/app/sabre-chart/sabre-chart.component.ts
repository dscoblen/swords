import { Component, OnInit } from '@angular/core';
import { SwordInfo, GoogleSheetService } from '../google-sheet.service';

interface Field {
  key: string;
  label: string;
}

@Component({
  selector: 'app-sabre-chart',
  templateUrl: './sabre-chart.component.html',
  styleUrls: ['./sabre-chart.component.scss']
})
export class SabreChartComponent implements OnInit {
  rawData: SwordInfo[] = [];
  data: any[] = [];
  max = { x: 0, y: 0 };
  min = { x: Number.MAX_SAFE_INTEGER, y: Number.MAX_SAFE_INTEGER };

  categories: Field[] = [
    { key: 'hiltType', label: 'Hilt Type' },
    { key: 'bladeType', label: 'Blade Type' },
    { key: 'bladeMaker', label: 'Blade Maker' },
    { key: 'country', label: 'Country' },
    { key: 'owner', label: 'Owner' }
  ];

  measurements: Field[] = [
    { key: 'length.blade', label: 'Blade Length (cm)' },
    { key: 'length.overall', label: 'Total Length (cm)' },
    { key: 'length.fuller', label: 'Fuller Length (cm)' },
    { key: 'pob', label: 'Point of Balance (cm)' },
    { key: 'weight.total', label: 'Total Weight (g)' },
    { key: 'weight.blade', label: 'Blade Weight (g)' },
    { key: 'weight.guard', label: 'Guard Weight (g)' },
    { key: 'weight.backstrap', label: 'Backstrap Weight (g)' },
    { key: 'weight.grip', label: 'Grip Weight (g)' },
    { key: 'weight.ferrule', label: 'Ferrule Weight (g)' },
    { key: 'weight.nut', label: 'Nut Weight (g)' },
    { key: 'weight.pad', label: 'Pad Weight (g)' },
    { key: 'curvature.amount', label: 'Curvature Amount (mm)' },
    { key: 'curvature.position', label: 'Curvature Position (cm)' },
    { key: 'width.ricasso', label: 'Ricasso Width (mm)' },
    { key: 'width.tip', label: 'Tip Width (mm)' },
    { key: 'width.halfway', label: 'Midpoint Width (mm)' },
    { key: 'thickness.ricasso', label: 'Ricasso Thickness (mm)' },
    { key: 'thickness.tip', label: 'Tip Thickness (mm)' },
    { key: 'thickness.halfway', label: 'Midpoint Thickness (mm)' },
    { key: 'thickness.atCurvature', label: 'Thickness at Curvature (mm)' },
    { key: 'thickness.atGuard', label: 'Guard Thickness (mm)' }
  ];

  selectedCategory: Field = this.categories[3];
  selectedXField: Field = this.measurements[4];
  selectedYField: Field = this.measurements[0];

  indices: string[] = ['images', 'name', 'notes'];

  constructor(private sheets: GoogleSheetService) {}

  private getValue(sword: SwordInfo, key: string): any {
    const keys = key.split('.');
    let value: any = sword;
    keys.forEach(token => (value = value[token]));
    return value;
  }

  prepareData() {
    const seriesMap = {};
    const max = { x: 0, y: 0 };
    const min = { x: Number.MAX_SAFE_INTEGER, y: Number.MAX_SAFE_INTEGER };

    this.rawData.forEach(sword => {
      let category = this.getValue(sword, this.selectedCategory.key);
      if (!category) {
        category = 'Unknown';
      }
      category = category.trim();

      const x = this.getValue(sword, this.selectedXField.key);
      const y = this.getValue(sword, this.selectedYField.key);

      if (x && y) {
        if (!seriesMap[category]) {
          seriesMap[category] = [];
        }
        seriesMap[category].push({
          name: sword.name,
          y: y,
          x: x,
          r: 1
        });

        min.x = Math.min(min.x, x);
        min.y = Math.min(min.y, y);
        max.x = Math.max(max.x, x);
        max.y = Math.max(max.y, y);
      }
    });

    const series = [];
    for (const country in seriesMap) {
      if (country) {
        series.push({
          name: country,
          series: seriesMap[country]
        });
      }
    }

    const xRange = max.x - min.x;
    min.x = min.x - xRange * 0.05;
    max.x = max.x + xRange * 0.05;

    const yRange = max.y - max.y;
    min.y = min.y - yRange * 0.05;
    max.y = max.y + yRange * 0.05;

    this.min = min;
    this.max = max;
    console.log(min);

    this.data = series;
  }
  ngOnInit() {
    this.sheets
      .load(
        '2PACX-1vQwM22FMcakeETYrIpb3SU5zk3xK6AIFKdsO_yGCbmv_qE82Unubxi6AoCvpaLXJXi3XTAdKKogbENv'
      )
      .subscribe(swords => {
        this.rawData = swords;
        this.prepareData();
      });
  }
}
