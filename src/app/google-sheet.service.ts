import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

export interface SwordInfo {
  images?: string;
  name?: string;
  hiltType?: string;
  bladeType?: string;
  bladeMaker?: string;
  country?: string;
  owner?: string;
  length?: {
    blade?: number;
    overall?: number;
    fuller?: number;
  };
  pob?: number;
  weight?: {
    total?: number;
    blade?: number;
    guard?: number;
    backstrap?: number;
    grip?: number;
    ferrule?: number;
    nut?: number;
    pad?: number;
  };
  curvature?: {
    amount?: number;
    position?: number;
  };
  width?: {
    ricasso?: number;
    tip?: number;
    halfway?: number;
  };
  thickness?: {
    ricasso?: number;
    tip?: number;
    halfway?: number;
    atCurvature?: number;
    guard?: number;
  };
  notes?: string;
  imageFolder?: string;
}
const indices: string[] = [
  'images',
  'name',
  'hiltType',
  'bladeType',
  'bladeMaker',
  'country',
  'owner',
  'length.blade',
  'length.overall',
  'length.fuller',
  'pob',
  'weight.total',
  'weight.blade',
  'weight.guard',
  'weight.backstrap',
  'weight.grip',
  'weight.ferrule',
  'weight.nut',
  'weight.pad',
  'curvature.amount',
  'curvature.position',
  'width.ricasso',
  'width.tip',
  'width.halfway',
  'thickness.ricasso',
  'thickness.tip',
  'thickness.halfway',
  'thickness.atCurvature',
  'thickness.atGuard',
  'notes',
  'imageFolder'
];

const types = {
  'length.overall': 'number',
  'length.fuller': 'number',
  'length.blade': 'number',
  'weight.total': 'number',
  'weight.blade': 'number',
  'weight.guard': 'number',
  'weight.backstrap': 'number',
  'weight.grip': 'number',
  'weight.ferrule': 'number',
  'weight.nut': 'number',
  'weight.pad': 'number',
  'curvature.amount': 'number',
  'curvature.position': 'number',
  'width.ricasso': 'number',
  'width.tip': 'number',
  'width.halfway': 'number',
  'thickness.ricasso': 'number',
  'thickness.tip': 'number',
  'thickness.halfway': 'number',
  'thickness.atCurvature': 'number',
  'thickness.atGuard': 'number',
  'pob': 'number'
};

@Injectable()
export class GoogleSheetService {
  constructor(private http: Http) {}

  public load(id: string) {
    // const url = 'https://spreadsheets.google.com/feeds/list/' + id + '/od6/public/values?alt=json';
    const url =
      'https://docs.google.com/spreadsheets/d/e/' + id + '/pub?output=tsv';
    return this.http
      .get(url)
      .map(res => res.text())
      .map(text => text.split('\n'))
      .map(lines => {
        // ",Saber,Hilt type,Blade Type,Blade Maker,Country,Owner,Length (cm),,,PoB (cm),Weight (g),,,,,,,,Curvature,,Width (mm),,,
        // Thickness (mm),,,,,Notes:"
        // ",,,,,,,Blade Length,Overall Length,Fuller Length,,Total Weight,Blade Weight,Guard Weight,Backstrap,
        // Grip,Ferrule,Nut,Pad,Amount (mm),Position (cm from hilt),Ricasso,Tip,Halfway,Ricasso,Tip,Halfway,At max curvature,Guard,
        const infos: SwordInfo[] = [];
        lines.forEach((line, index) => {
          if (index > 1) {
            const info: SwordInfo = {
              length: {},
              weight: {},
              curvature: {},
              width: {},
              thickness: {}
            };
            line.split('\t').forEach((token, pos) => {
              const key: string = indices[pos];
              const value = this.parseValue(token, key);
              if (key && !key.includes('.')) {
                info[key] = value;
              } else {
                const keys = key.split('.');
                info[keys[0]][keys[1]] = value;
              }
            });
            if (info.owner) {
              infos.push(info);
            }
          }
        });
        return infos;
      });
  }
  parseValue(token: string, key: string) {
    if (types[key] === 'number') {
      try {
        const val = Number.parseFloat(token);
        return isNaN(val) ? undefined : val;
      } catch (e) {
        return null;
      }
    } else {
      return token;
    }
  }
}
