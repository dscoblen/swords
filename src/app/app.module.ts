import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import { ClarityModule } from '@clr/angular';
import {NgxChartsModule} from '@swimlane/ngx-charts';


import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { GoogleSheetService } from './google-sheet.service';
import { SabreTableComponent } from './sabre-table/sabre-table.component';
import { SabreChartComponent } from './sabre-chart/sabre-chart.component';


@NgModule({
  declarations: [
    AppComponent,
    SabreTableComponent,
    SabreChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgxChartsModule,
    AppRoutingModule,
    ClarityModule,
    HttpModule,
    BrowserAnimationsModule
  ],
  providers: [GoogleSheetService],
  bootstrap: [AppComponent]
})
export class AppModule { }
