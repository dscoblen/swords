import { Component, OnInit } from '@angular/core';
import { GoogleSheetService, SwordInfo } from '../google-sheet.service';

@Component({
  selector: 'app-sabre-table',
  templateUrl: './sabre-table.component.html',
  styleUrls: ['./sabre-table.component.scss']
})
export class SabreTableComponent implements OnInit {
  swords: SwordInfo[] = [];
  constructor(private sheets: GoogleSheetService) {}
  ngOnInit() {
    this.sheets
      .load(
        '2PACX-1vQwM22FMcakeETYrIpb3SU5zk3xK6AIFKdsO_yGCbmv_qE82Unubxi6AoCvpaLXJXi3XTAdKKogbENv'
      )
      .subscribe(swords => {
        this.swords = swords;
        console.log(swords);
      });
  }
}
